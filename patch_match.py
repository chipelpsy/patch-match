#!/bin/python

import numpy as np
import cv2 as cv
import random

def patch_match(im1, im2, patch_w=5, num_iter=5):
    ann = np.empty(im1.shape[:2] + (2,), dtype='int32')
    annd = np.empty(im1.shape[:2])
    w1, h1 = [x - patch_w + 1 for x in im1.shape[:2]]
    w2, h2 = [x - patch_w + 1 for x in im2.shape[:2]]
    for i in range(w1):
        for j in range(h1):
            x = random.randint(0, w2 - 1)
            y = random.randint(0, h2 - 1)
            ann[i, j] = (x, y)
            annd[i, j] = patch_dist(im1, im2, i, j, x, y, patch_w)
    ann_iter(im1, im2, ann, annd, patch_w, num_iter)
    return (ann, annd)


def ann_iter(im1, im2, ann, annd, patch_w=5, num_iter=5):
    w1, h1 = [x - patch_w + 1 for x in im1.shape[:2]]
    w2, h2 = [x - patch_w + 1 for x in im2.shape[:2]]
    for iter in range(num_iter):
        if iter%2:
            x_start, x_end, x_inc = 0, w1, 1
            y_start, y_end, y_inc = 0, h1, 1
        else:
            x_start, x_end, x_inc = w1 - 1, -1, -1
            y_start, y_end, y_inc = h1 - 1, -1, -1
        for i in range(x_start, x_end, x_inc):
#            print((i - x_start)*100/(x_end - x_start))
            for j in range(y_start, y_end, y_inc):
                x_best, y_best = ann[i, j]
                d_best = annd[i, j]

                if 0 <= i - x_inc < w1:
                    x, y = ann[i - x_inc, j]
                    x += x_inc
                    if 0 <= x < w2:
                        d = patch_dist(im1, im2, i, j, x, y, patch_w)
                        if d < d_best:
                            x_best, y_best, d_best = x, y, d

                if 0 <= j - y_inc < h1:
                    x, y = ann[i, j - y_inc]
                    y += y_inc
                    if 0 <= y < h2:
                        d = patch_dist(im1, im2, i, j, x, y, patch_w)
                        if d < d_best:
                            x_best, y_best, d_best = x, y, d

                mag = max(im2.shape)
                while mag > 0:
                    x_min, x_max = max(x_best - mag, 0), min(x_best + mag, w2 - 1)
                    y_min, y_max = max(y_best - mag, 0), min(y_best + mag, h2 - 1)
                    x = random.randint(x_min, x_max)
                    y = random.randint(y_min, y_max)
                    d = patch_dist(im1, im2, i, j, x, y, patch_w)
                    if d < d_best:
                        x_best, y_best, d_best = x, y, d
                    mag //= 2

                ann[i, j] = (x_best, y_best)
                annd[i, j] = d_best

def patch_dist(im1, im2, x1, y1, x2, y2, patch_w):
    toret = 0
    x1, x2, y1, y2, patch_w = map(int, (x1, x2, y1, y2, patch_w))
    p_nar = np.array(range(patch_w))
    p1 = im1[np.ix_(x1 + p_nar, y1 + p_nar)]
    p2 = im2[np.ix_(x2 + p_nar, y2 + p_nar)]
    diff = p1.astype('int32') - p2.astype('int32')
    toret += np.linalg.norm(diff, axis=2).sum()
    return toret
