import numpy as np
import cv2 as cv
import patch_match

deBug = True

def hole_fill(im, mask, r=5):
    src_pyramid = []
    msk_pyramid = []
    src = im
    msk = mask.astype('uint8')
    num_lvls = 0
    if deBug: print('Building pyramid: ', end='')
    while np.all([x > r for x in src.shape[:2]]):
        if deBug: print(num_lvls, ' ', end='')
        src_pyramid.append(src)
        msk_pyramid.append(msk)
        num_lvls += 1
        src = cv.pyrDown(src)
        msk = cv.pyrDown(msk)
    if deBug: print('done')

    if deBug: print('Main loop')
    for lvl in range(num_lvls - 1, -1, -1):
        if deBug: print('Processing level {}'.format(lvl))
        src = src_pyramid[lvl]
        msk = msk_pyramid[lvl]

        if lvl == num_lvls - 1:
            target = src.copy()
            msk = np.zeros(target.shape[:2])
            st = patch_match.patch_match(src, target, r)
            ts = patch_match.patch_match(target, src, r)
        else:
            old_tgetr = target.copy()
            cv.imshow('resize', old_tgetr/255)
            target = cv.resize(target, src.shape[:2][::-1])
            #            target[msk == 0] = src[msk == 0]
            cv.imshow('resizeafter', target/255)
            cv.waitKey()
            def restimate_ann(src, target, annt, sh):
                o_sh = annt[1].shape
                ann = np.empty(sh + (2,), dtype='int32')
                annd = np.empty(sh)
                x_rat = sh[0]/o_sh[0]
                y_rat = sh[1]/o_sh[1]
                for i in range(sh[0]):
                    for j in range(sh[1]):
                        x = min(int(i//x_rat), o_sh[0] - 1)
                        y = min(int(j//y_rat), o_sh[1] - 1)
                        ann[i, j, 0] = min(int(annt[0][x, y, 0]*x_rat), sh[0]-r-1)
                        ann[i, j, 1] = min(int(annt[0][x, y, 0]*x_rat), sh[1]-r-1)
                        annd[i, j] = annt[1][x, y]
                        if 0<= i < sh[0] - r and 0 <= j < sh[1] - r:
                            annd[i, j] = patch_match.patch_dist(src, target, i, j, ann[i, j, 0], ann[i, j, 1], r)
                return (ann, annd)
                        
            st = restimate_ann(src, target, st, src.shape[:2])
            ts = restimate_ann(target, src, ts, src.shape[:2])

        target = EM(src, target, msk, st, ts, r, 1 + 2*lvl)

    return target

def EM(src, target, msk, st, ts, r=5, num_iter=7):
    w, h = src.shape[:2]
    def expectation():
        src_int = src.astype('int64')
        vote = np.zeros(src.shape[:2] + (4,), dtype='float64')
        w_st = np.exp(-st[1]/10)  #np.ones(st[1].shape)
        w_ts = np.exp(-ts[1]/10)  #np.ones(ts[1].shape)
        for i in range(w):
            if deBug: print('\t{}/{}'.format(i, w))
            for j in range(h):
                x_st, y_st = st[0][i, j]
                w_st = st[1][i, j]
                x_ts, y_ts = ts[0][i, j]
                w_ts = ts[1][i, j]
                vote[i, j, :3] += 0.001*target[i, j, :]
                vote[i, j, 3] += 0.001
                for dx in range(r):
                    for dy in range(r):
                        x_s, y_s = i + dx, j + dy
                        x_t, y_t = x_st + dx, y_st + dy
                        x_s, y_s, x_t, y_t = map(int, (x_s, y_s, x_t, y_t))
                        if 0 <= x_s < w and 0 <= x_t < w and 0 <= y_s < h and 0 <= y_t < h :#and not msk[x_s, y_s]:
                            vote[x_t, y_t, :3] += w_st*src_int[x_s, y_s, :]
                            vote[x_t, y_t, 3] += w_st
                        x_t, y_t = i + dx, j + dy
                        x_s, y_s = x_ts + dx, y_ts + dy
                        x_s, y_s, x_t, y_t = map(int, (x_s, y_s, x_t, y_t))
                        if 0 <= x_s < w and 0 <= x_t < w and 0 <= y_s < h and 0 <= y_t < h :#and not msk[x_s, y_s]:
                            vote[x_t, y_t, :3] += w_ts*src_int[x_s, y_s, :]
                            vote[x_t, y_t, 3] += w_ts
        return vote

    def maximization(vote):
        replacer = (vote[:, :, :3]/np.dstack([vote[:, :, 3] for i in range(3)])).astype('uint8')
        return replacer  #np.dstack([replacer[:, :, i]*msk + src[:, :, i]*(1 - msk) for i in range(3)])

    for iter_no in range(num_iter):
        if deBug: print('EM({}/{})'.format(iter_no + 1, num_iter))
        for i in range(w):
            for j in range(h):
                if not msk[i, j]:
                    st[0][i, j] = [i, j]
                    st[1][i, j] = 0
                    ts[0][i, j] = [i, j]
                    ts[1][i, j] = 0
        patch_match.ann_iter(src, target, st[0], st[1], r)
        patch_match.ann_iter(target, src, ts[0], ts[1], r)
        votes = expectation()
        target = maximization(votes)
        cv.imshow('target', target/255)
        cv.waitKey(1)

    return target
