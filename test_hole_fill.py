import numpy as np
import cv2 as cv
import hole_fill
import pickle

im = cv.imread('bricks_im.png')
mask = cv.imread('bricks_mask.png')
#im = cv.imread('forest.bmp')
#mask = cv.imread('mask.bmp')

mask = mask[:, :, 0]
im[mask > 1] = [0, 0, 0]

op = hole_fill.hole_fill(im, mask, 5)

cv.imshow('win', op/255)
cv.waitKey()
cv.imwrite('hole_0.png', op)

pickle.dump(op, open('hole_small.pkl', 'wb'))
