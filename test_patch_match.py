import numpy as np
import cv2 as cv
import patch_match
import pickle

im1 = cv.imread('q1.jpg')
im2 = cv.imread('q2.jpg')

ann, annd = patch_match.patch_match(im1, im2)

cv.imshow('win', np.dstack([ann[:, :, 0], ann[:, :, 1], ann[:, :, 0]]))
cv.waitKey()
cv.imwrite('ann.png', np.dstack([ann[:, :, 0], ann[:, :, 1], ann[:, :, 0]]))

pickle.dump([ann, annd], open('fnn_0.pkl', 'wb'))
