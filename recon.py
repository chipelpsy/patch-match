import numpy as np
import cv2 as cv
import patch_match
import pickle

no_xtras = True

ann ,annd = pickle.load(open('fnn_0.pkl', 'rb'))
im = cv.imread('bb2_xtra.png')

w, h = annd.shape
reim = np.empty((w, h, 3), dtype = 'uint8')
if no_xtras:
    for i in range(w):
        for j in range(h):
            reim[i, j, :] = im[ann[i, j, 0], ann[i, j, 1], :]

cv.imshow('reconstructed', reim)
cv.waitKey()

cv.imwrite('reconstructed.png', reim)

